
// station 保存在内存里面 防止重启后全部重新计算

let pro={
    updateRoom:function (room){
        if(!room.Memory)room.Memory = {}
        room.update();
        StationCarry.update(room);
        StationSources.update(room);
        StationUpgrade.update(room);
        StationTower.update(room);
        StationMineral.update(room);
        StationDefense.update(room);
    },
    firstActive : true,
    exec:function (room) {
        StationObserver.update(room);
        if((Game.time+room.hashCode())%31==0||pro.firstActive){
            HelperError.catchError(()=>pro.updateRoom(room))
            pro.firstActive = false;
        }

        if((Game.time+room.hashCode())%301==0||pro.firstActive) {
            BetterMove.deletePathInRoom(room.name);
        }
        if(!room.my) return; //如果不是自己的房子则不动


        /** updateRoom */


        // room.creeps("carrier").forEach(e=>e.memory.tasks=[])
        // room.creeps("worker").forEach(e=>e.memory.tasks=[])

        HelperError.catchError(()=>StationLab.exec(room));// 先预计算 labs

        HelperError.catchError(()=>StationFactory.exec(room));// 先预计算 factory

        HelperError.catchError(()=>StationTower.exec(room));
        /** strategy */

        if(room.flags("minSizeRoom").length) //minSizeRoom_W8N8
            HelperError.catchError(()=>StrategyMinSizeRoom.exec(room))
        else if(!room.storage||!room.storage.my||room.level<4)
            HelperError.catchError(()=>StrategyLowLevel.exec(room))
        else
            HelperError.catchError(()=>StrategyHighLevel.exec(room))


        HelperError.catchError(()=>StrategyResourceBalance.exec(room))

        HelperError.catchError(()=>StrategyOuterHarvest.exec(room))

        HelperError.catchError(()=>StrategyPillage.exec(room))

        HelperError.catchError(()=>StrategyMarket.exec(room))

        HelperError.catchError(()=>StationObserver.obOverRooms(room))

        HelperError.catchError(()=>StrategyDeposits.exec(room))

        HelperError.catchError(()=>StrategyPowerBank.exec(room))

        HelperError.catchError(()=>StationDefense.checkSafeMode(room))

        // if(room.name == "W8N3")log(room.name,StationLab.boostAble(room,{"KH":3000}))

        // if(room.name=="W19N11"&& Game.time%30 == 0 && (!Memory.test||!Memory.test.start||Memory.test.start<500)){
        //     let tasks = [
        //         UtilsTask.taskData("testEnd"),
        //         UtilsTask.taskData("moveCrossShardByPath",undefined,ops = {
        //             path : [
        //                 { shard: 'shard3', roomName: 'W20N10', x: 40, y: 16 },
        //                 { shard: 'shard2', roomName: 'W20N10', x: 39, y: 33 },
        //                 { shard: 'shard1', roomName: 'W20N10', x: 41, y: 9 },
        //                 { shard: 'shard0', roomName: 'W29N10', x: 1, y: 19 },
        //                 { shard: 'shard0', roomName: 'W30N30', x: 43, y: 43 },
        //                 { shard: 'shard1', roomName: 'W20N20', x: 38, y: 12 },
        //                 { shard: 'shard0', roomName: 'W30N39', x: 40, y: 1 },
        //                 { shard: 'shard0', roomName: 'W19N40', x: 1, y: 6 },
        //                 { shard: 'shard0', roomName: 'W20N50', x: 43, y: 29 },
        //                 { shard: 'shard1', roomName: 'W10N30', x: 10, y: 13 },
        //                 { shard: 'shard0', roomName: 'W19N60', x: 1, y: 4 },
        //                 { shard: 'shard0', roomName: 'W20N70', x: 39, y: 38 },
        //                 { shard: 'shard1', roomName: 'W10N40', x: 8, y: 25 },
        //                 { shard: 'shard0', roomName: 'W20N79', x: 35, y: 1 },
        //                 { shard: 'shard0', roomName: 'E10N80', x: 36, y: 18 },
        //                 { shard: 'shard1', roomName: 'E10N40', x: 33, y: 6 },
        //                 { shard: 'shard0', roomName: 'E20N82', x: 29, y: 48 },
        //                 { shard: 'shard0', roomName: 'E70N80', x: 33, y: 8 },
        //                 { shard: 'shard1', roomName: 'E40N40', x: 40, y: 19 },
        //                 { shard: 'shard0', roomName: 'E80N69', x: 5, y: 1 },
        //                 { shard: 'shard0', roomName: 'E40N71', x: 28, y: 48 },
        //                 { shard: 'shard0', roomName: 'E50N70', x: 14, y: 38 },
        //                 { shard: 'shard1', roomName: 'E30N40', x: 8, y: 33 },
        //                 { shard: 'shard2', roomName: 'E30N40', x: 13, y: 29 }
        //             ]
        //         }),
        //         UtilsTask.taskData("testStart")
        //     ]
        //     StationHive.trySpawn(room,room.name,[MOVE],"scouterOverShard",tasks)
        // }

        // log(_.keys(BetterMove.creepPathCache).map(e=>Game.creeps[e].memory.role))
        // room.creeps().forEach(e=>{
        //     if(BetterMove.creepPathCache[e.name]){
        //         // HelperVisual.commonText(e,_.values(BetterMove.creepPathCache[e.name]["path"]))
        //         // log(_.values(BetterMove.creepPathCache[e.name]["path"]["posArray"]))
        //         // room.visual.poly(BetterMove.creepPathCache[e.name]["path"]["posArray"])
        //         // log(BetterMove.creepPathCache[e.name]["path"]["posArray"])
        //     }
        // })
        // let path = PathFinder.search(room.storage.pos,room.source[0].pos)
        // log(path)
        // room.visual.poly(path.path)
    }
};


global.ManagerRooms=pro;