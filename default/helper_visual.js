
global.structuresShape= {
    "spawn": "◎",
    "extension": "ⓔ",
    "link": "◈",
    "road": "•",
    "constructedWall": "▓",
    "rampart": "⊙",
    "storage": "▤",
    "tower": "🔫",
    "observer": "👀",
    "powerSpawn": "❂",
    "extractor": "☸",
    "terminal": "✡",
    "lab": "☢",
    "container": "□",
    "nuker": "▲",
    "factory": "☭"
}
global.structuresColor= {
    "spawn": "cyan",
    "extension": "#0bb118",
    "link": "yellow",
    "road": "#fa6f6f",
    "constructedWall": "#003fff",
    "rampart": "#003fff",
    "storage": "yellow",
    "tower": "cyan",
    "observer": "yellow",
    "powerSpawn": "cyan",
    "extractor": "cyan",
    "terminal": "yellow",
    "lab": "#d500ff",
    "container": "yellow",
    "nuker": "cyan",
    "factory": "yellow"
}
pro={
    //线性同余随机数
    rnd : function( seed ){
        return ( seed * 9301 + 49297 ) % 233280; //为何使用这三个数?
    },
    // seed 的随机颜色
    randomColor : function (seed){
        seed = parseInt(seed)
        let str = "12334567890ABCDEF"
        let out = "#"
        for(let i=0;i<6;i++){
            seed = pro.rnd(seed+Game.time%100)
            out+=str[parseInt(seed)%str.length]
        }
        return out
    },
    roomMap:{},
    getRoomVisual:function (roomNameOrObj){
        let roomName = roomNameOrObj.name||roomNameOrObj
        if(!pro.roomMap[roomName]){
            pro.roomMap[roomName] = new RoomVisual(roomNameOrObj.name||roomNameOrObj)
        }
        return pro.roomMap[roomName]
    },
    // 大概消耗1 CPU！ 慎用！
    showRoomStructures : function (roomName,structMap){
        let roomStructs = new RoomArray().init()
        const visual = new RoomVisual(roomName);
        if(structMap["road"])
            Utils.decodePosArray(structMap["road"]).forEach(e=>roomStructs.set(e.x,e.y,"road"))
        _.keys(CONTROLLER_STRUCTURES).forEach(struct=>{
            if(struct=="road"){
                Utils.decodePosArray(structMap[struct]).forEach(e=>{
                    roomStructs.forNear((x,y,val)=>{
                        if(val =="road"&&((e.x>=x&&e.y>=y)||(e.x>x&&e.y<y)))visual.line(x,y,e.x,e.y,{color:structuresColor[struct]})
                    },e.x,e.y);
                    visual.text(structuresShape[struct], e.x,e.y+0.25, {color: structuresColor[struct],opacity:0.75,fontSize: 7})
                })
            }
            else Utils.decodePosArray(structMap[struct]).forEach(e=>visual.text(structuresShape[struct], e.x,e.y+0.25, {color: structuresColor[struct],opacity:0.75,fontSize: 7}))
        })
    },
    showText:function (roomNameOrObj , text, objOrPos, color='red') {
        if(roomNameOrObj.pos){
            objOrPos = roomNameOrObj.pos
            roomNameOrObj= objOrPos.roomName
        }
        let visual = pro.getRoomVisual(roomNameOrObj)
        let pos=objOrPos.pos||objOrPos
        visual.text(text, pos.x,pos.y+0.25, {color: color,opacity:0.75,fontSize: 7})
    },
    mapShowText:function (roomNameOrObj , text, objOrPos, color='red') {
        if(roomNameOrObj.pos){
            objOrPos = roomNameOrObj.pos
            roomNameOrObj= objOrPos.roomName
        }
        // let visual = pro.getRoomVisual(roomNameOrObj)
        let pos=objOrPos.pos||objOrPos
        Game.map.visual.text(text, pos, {color: color,opacity:0.75,fontSize: 7})
    },
    showPath:function (posArray, color='red') {
        let roomNameMap = {}
        posArray.forEach(e=>{
            if (!roomNameMap[e.roomName]) {
                roomNameMap[e.roomName] = []
            }
            roomNameMap[e.roomName].push(e)
        })
        _.keys(roomNameMap).forEach(k=>{
            pro.getRoomVisual(k).poly(roomNameMap[k],{stroke : color})
        })
        // let visual = pro.getRoomVisual(roomNameOrObj)
        // let pos=objOrPos.pos||objOrPos
        // visual.text(text, pos.x,pos.y+0.25, {color: color,opacity:0.75,fontSize: 7})
    },
}

global.HelperVisual=pro