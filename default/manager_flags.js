
let pro={
    globalFlags:[],
    init:function(){
        pro.globalFlags = []

        for (let name in Memory.flags) {
            if (!Game.flags[name]) {
                delete Memory.flags[name];
            }
        }

        let flagRoomMap = {}

        for (let name in Game.flags) {
            let strLs = name.split("_");
            let prefix = strLs[0]
            let roomName = strLs.length>=1?strLs[1]:undefined
            let room = Game.rooms[roomName]

            if(room){
                flagRoomMap[roomName] = flagRoomMap[roomName]||[]
                flagRoomMap[roomName].push(Game.flags[name])
            }
        }

        for(let name in flagRoomMap){
            Game.rooms[name].setFlagList(flagRoomMap[name])
        }


    },

};


global.ManagerFlags = pro;