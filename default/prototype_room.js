

Room.prototype.setCreepsList=function (list) {
    this._creepsList = list||[]
};

Room.prototype.creeps=function (role,spawned=true) {
    if(this._creepsList == null)this._creepsList = []
    if(role){
        let creeps = this[role+"_creepsList"];
        if(null === creeps){
            creeps = this._creepsList.filter(e=>e.memory.role == role);
            this[role+"_creepsList"] = creeps;
        }
        if(spawned){
            if(!this[role+"_creepsListSpawned"])this[role+"_creepsListSpawned"] = creeps.filter(e=>e.spawning!=true);
            return this[role+"_creepsListSpawned"];
        }
        else return creeps
    }
    else {
        if(spawned){
            if(!this._creepsListSpawned)this._creepsListSpawned = this._creepsList.filter(e=>e.spawning!=true);
            return this._creepsListSpawned;
        }
        else return this._creepsList;
    }
};

Room.prototype.setFlagList=function (list) {
    this._flagList = list||[]
};

Room.prototype.flags=function (prefix) {
    if(prefix){
        this._flagList = this._flagList||[]
        let flags = this[prefix+"_flagList"];
        if(null === flags){
            flags = this._flagList.filter(e=>e.getPrefix() == prefix);
            this[prefix+"_flagList"] = flags;
        }
        return flags
    }
    else {
        return this._flagList;
    }
};


/** roomName 的哈希值 */
Room.prototype.hashCode=function () {
    if(this.memory.hashCode)return this.memory.hashCode
    let hash = Utils.rnd(Utils.hashCode(this.name))
    this.memory.hashCode = hash;
    return hash;
};

Room.prototype.getEnergyCapacityAvailable=function (){
    return this.energyCapacityAvailable
}

Room.prototype.getEnergyAvailable=function () {
    return this.energyAvailable
};