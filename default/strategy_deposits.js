
let MAX_COOL_DOWM = 100



Creep.prototype.registerHarvestDepositCarrier=function () {
    let headTask = this.headTask();
    let flag = Game.flags[headTask.flagName];
    if(flag){
        flag.memory.carriers = flag.memory.carriers || []
        if(!flag.memory.carriers.contains(this.id)){
            flag.memory.carriers.push(this.id)
        }
    }
};


Creep.prototype.harvestDeposit=function () {
    let task= this.headTask();
    if(task.roomName!=this.room.name){
        this.goTo(task);
    }else{
        let deposit=Game.getObjectById(task["id"]);
        if(this.freeCapacity()<this.getPartCnt(WORK)){

        }
        else if(deposit && this.harvest(deposit) == ERR_NOT_IN_RANGE){
            this.goTo(deposit)
        }
        if(deposit&&!this.memory.concated&&this.pos.inRangeTo(deposit,3))this.concatDeposit()
    }
};

Creep.prototype.registerHarvestDeposit=function () {
    let headTask = this.headTask();
    let flag = Game.flags[headTask.flagName];
    if(flag){
        flag.memory.harvesters = flag.memory.harvesters || []
        if(!flag.memory.harvesters.contains(this.id)){
            flag.memory.harvesters.push(this.id)
        }
    }
};

Creep.prototype.concatDeposit=function () {
    let headTask = this.headTask();
    this.memory.concated = true
    let flag = Game.flags[headTask.flagName];
    let pathTime = 1500 - this.ticksToLive;
    if(flag && (!flag.memory.pathTime||flag.memory.pathTime>pathTime)) {
        flag.memory.pathTime = pathTime
    }
};

Creep.prototype.harvestDeposit=function () {
    let task= this.headTask();
    // this.suicide()
    if(task.roomName!=this.room.name){
        this.goTo(task);
    }else{
        let deposit=Game.getObjectById(task["id"]);
        if(this.freeCapacity()<this.getPartCnt(WORK)){

        }
        else if(deposit && this.harvest(deposit) == ERR_NOT_IN_RANGE){
            this.goTo(deposit)
        }
        if(deposit&&this.pos.isNearTo(deposit)&&!this.memory.concated)this.concatDeposit()
    }
};

Creep.prototype.carryDeposit=function () {
    let task= this.headTask();
    if(task.roomName!=this.room.name){
        this.goTo(task);
    }else{
        let flag = Game.flags[task.flagName];
        if(!flag)return;
        if(this.ticksToLive<(flag.memory.pathTime||600)*1.2 || this.storeFull() ){// 回家
            flag.memory.carriers = flag.memory.carriers || []
            if(!flag.memory.carriers.contains(this.id))
                flag.memory.carriers = flag.memory.carriers.without(this.id)
            this.popTask();
            this.addTask([UtilsTask.taskData("recycleCreep")])
            this.fillAllMainRoomStorage();
            this.execLastTask();
        }

        if(this.ticksToLive%20==0){
            let tombstone = this.pos.findClosestByPath(FIND_TOMBSTONES,{filter: e=>e.store.getResTypeList().length,range:3});
            this.carryAll(tombstone)
        }

        let needTransferCreep = flag.memory.harvesters.map(id=>Game.getObjectById(id)).filter(e=>e&&e.storeUsed()>=20).head();
        if(this.pos.isNearTo(needTransferCreep))needTransferCreep.transfer(this,needTransferCreep.store.getResTypeList()[0]);
        else this.moveTo(needTransferCreep)

        if(!needTransferCreep&&flag&&this.pos.getRangeTo(flag)>3){
            this.moveTo(flag);
        }
    }
};

let pro = {

    createOrUpdateDepositMission:function(targetRoomName, depositData){//
        let spawnRoomName = StationObserver.getClosedMyRoomName(targetRoomName);
        let flagName = "deposit_"+spawnRoomName+"_"+depositData.x+"_"+depositData.y;
        if(!Memory.flags[flagName])Memory.flags[flagName] = {}
        let flagMemory = Memory.flags[flagName];
        depositData.flagName = flagName;
        depositData.roomName = targetRoomName;
        for(let k in depositData){
            flagMemory[k] = depositData[k] ;
        }
        if(!spawnRoomName||depositData.lastCooldown>MAX_COOL_DOWM)return;
        if(!Game.flags[flagName]){
            (new RoomPosition(depositData.x,depositData.y,targetRoomName)).createFlag(flagName)
        }
    },
    trySpawnHarDeposits:function (room,memory){
        let body = ManagerCreeps.calcBodyPart({  [WORK]: 22 ,[CARRY]: 6,[MOVE]: 22 })
        StationHive.trySpawn(room,room.name,body,"harDeposits",
            [UtilsTask.taskData("harvestDeposit","registerHarvestDeposit",memory)]
        )
    },
    trySpawnCarryDeposits:function (room,memory){
        let body = []
        for(let i=0;i<25;i++)
            body.push(CARRY,MOVE)
        StationHive.trySpawn(room,room.name,body,"carrierDeposits",
            [UtilsTask.taskData("carryDeposit","registerHarvestDepositCarrier",memory)]
        )
    },
    exec:function (room) {
        if((Game.time+room.hashCode()) % 3 != 0) return;
        room.flags("deposit").forEach(flag=>{
            if(!flag.memory.harvesters)flag.memory.harvesters = []
            else flag.memory.harvesters = flag.memory.harvesters.filter(id=>Game.getObjectById(id))
            if(!flag.memory.carriers) flag.memory.carriers = []
            else flag.memory.carriers = flag.memory.carriers.filter(id=>{
                let creep = Game.getObjectById(id)
                if(creep) return creep.headTask()&&creep.headTask().taskName=="carryDeposit"
                return false
            });
            if(Game.time<flag.memory.disappearTime&&flag.memory.lastCooldown<MAX_COOL_DOWM){
                if(!flag.memory.walkableAroundCnt){
                    flag.memory.walkableAroundCnt = flag.pos.walkableAroundCnt()
                }
                let ttlCreepCnt = flag.memory.harvesters.map(id=>Game.getObjectById(id)).filter(e=>e.spawning||e.ticksToLive>(flag.memory.pathTime||0)+150).length
                if(ttlCreepCnt<flag.memory.walkableAroundCnt){
                    pro.trySpawnHarDeposits(room,flag.memory);
                }
                ttlCreepCnt = flag.memory.carriers.map(id=>Game.getObjectById(id)).filter(e=>e.spawning||e.ticksToLive>(flag.memory.pathTime||0)+150).length
                if(ttlCreepCnt<1){
                    pro.trySpawnCarryDeposits(room,flag.memory);
                }
            }else if(flag.memory.harvesters.length==0&&flag.memory.carriers.length==0){
                flag.remove();
            }else if(flag.memory.harvesters.length!=0&&flag.memory.carriers.length==0){
                pro.trySpawnCarryDeposits(room,flag.memory);// 如果还有挖矿继续派兵
            }
            // HelperVisual.mapShowText(flag,flag.name)
        })
    }

}


global.StrategyDeposits=pro;