/*
工厂分为3个阶段，和lab类似：
搬运请求
合成
清理

合成分为 无等级和有等级两种
无等级合成方案为
先凑齐 3000 数量的资源，3000为一批
然后是 原矿
如果压缩的资源足够的情况下，优先拿库存的

 */

let STAT_CLEAR = "clear"
let STAT_FILL = "fill"
let STAT_PRODUCE = "produce"
let FACTORY_SLEEP_TIME = 20


let BASE_DEPOSITS = [RESOURCE_SILICON,RESOURCE_BIOMASS,RESOURCE_METAL,RESOURCE_MIST];
let BASE_COMMODITIES_MAP = {
    [RESOURCE_SILICON]:RESOURCE_WIRE,
    [RESOURCE_BIOMASS]:RESOURCE_CELL,
    [RESOURCE_METAL]:RESOURCE_ALLOY,
    [RESOURCE_MIST]:RESOURCE_CONDENSATE
};
let BASE_BATCH_SIZE =  30  // 无等级合成的批次

Creep.prototype.registerFactoryCarry=function () {
    this.mainRoom()._carryFactory = true;
};


let pro={
    stationName:"stationFactory",
    checkNoLevelRes:function(room,res,amount) {
        let array = []
        let produceTimes = Math.ceil(amount/COMMODITIES[res].amount);
        for(let t in COMMODITIES[res].components){
            let existCnt = StationCarry.roomMassStoreCnt(room,t);
            let resNeedAll = COMMODITIES[res].components[t]*produceTimes;
            // log(t,amount,produceTimes,COMMODITIES[res].components[t],resNeedAll,produceTimes*COMMODITIES[res].amount)
            if(existCnt>=resNeedAll) {
                array.push([t, resNeedAll]);
            } else if(t.indexOf("bar")>=0){
                return pro.checkNoLevelRes(room,t,produceTimes*COMMODITIES[res].amount)
            } else{
                return undefined;
            }
        }
        return {produceResType:res,needs:array}
    },
    noLevel:function (room){
        for(let com of BASE_DEPOSITS){
            if (StationCarry.roomMassStoreCnt(room,com) >= BASE_BATCH_SIZE) {
                let produce = BASE_COMMODITIES_MAP[com];
                let out = pro.checkNoLevelRes(room,produce,BASE_BATCH_SIZE*COMMODITIES[produce].amount)
                if(out)return out;
            }
        }
        //
    },
    getProduceType:function (room){
        let noLevel = pro.noLevel(room);
        if(noLevel)return noLevel;
    },
    generatorFillTask:function (room){
        if(room._carryFactory||!room.factory)return[];
        let sm =  room.memory[pro.stationName];
        if(sm.stat==STAT_FILL){
            let tasks = []
            for(let resCntPair of sm.produce.needs){
                let factoryCnt = room.factory.store[resCntPair[0]]||0;
                if(factoryCnt<resCntPair[1]){
                    tasks.push(UtilsTask.task(room.factory,"fillRes","registerFactoryCarry",{resType:resCntPair[0]}));
                    tasks.push(...StationCarry.generatorMassStoreCarry(room,resCntPair[0],resCntPair[1]-factoryCnt));
                }
            }
            return tasks
        }
        if(sm.stat==STAT_CLEAR){
            return [UtilsTask.task(room.storage,"fillAllTask","registerFactoryCarry"),
                UtilsTask.task(room.factory,"carryAllTask","registerFactoryCarry")]
        }
        return [];
    },
    exec:function (room){
        if (!room.factory) return;
        let sm = room.memory[pro.stationName];
        if(!sm)sm = room.memory[pro.stationName] = {}
        if(sm.lastCooldown)sm.lastCooldown-=1
        if(!sm.stat) sm.stat = STAT_CLEAR
        if (!sm.lastCooldown) {
            // log(pro.generatorFillTask(room))
            // HelperVisual.showText(room.factory,sm.stat)
            if(sm.stat == STAT_CLEAR){// 清理阶段
                if(room.factory.store.getAllResTypeCount()==0){
                    let needProduce = pro.getProduceType(room)
                    if(!needProduce){
                        sm.lastCooldown = FACTORY_SLEEP_TIME;
                    }else{
                        sm.produce = needProduce
                        sm.stat = STAT_FILL
                    }
                }
            }else if(sm.stat == STAT_FILL){// 搬运阶段
                if(!sm.produce){
                    sm.stat = STAT_CLEAR
                    return;
                }
                let fullCnt = 0
                for(let resCntPair of sm.produce.needs){
                    let factoryCnt = room.factory.store[resCntPair[0]]||0;
                    if(factoryCnt>=resCntPair[1]){
                        fullCnt += 1;
                    }
                    if(!room._carryFactory&&StationCarry.roomMassStoreCnt(room,resCntPair[0])+factoryCnt<resCntPair[1]){
                        sm.stat = STAT_CLEAR
                        return;
                    }
                }
                if (sm.produce.needs.length == fullCnt) {
                    sm.stat = STAT_PRODUCE
                }
                // log(room.memory[pro.stationName].produce)
            }else if(sm.stat == STAT_PRODUCE) {// 反应阶段
                let resType = sm.produce.produceResType
                let code = room.factory.produce(resType)

                if(code==OK){
                    sm.lastCooldown = COMMODITIES[resType].cooldown
                    let type = _.keys(COMMODITIES[resType].components)[0];
                    if(room.factory.store[type] == COMMODITIES[resType].components[type]){
                        sm.lastCooldown = 1;
                        sm.stat = STAT_CLEAR
                    }
                }else if(code!=ERR_TIRED){
                    sm.stat = STAT_CLEAR
                }
            }
        }


    },


};



global.StationFactory=pro;