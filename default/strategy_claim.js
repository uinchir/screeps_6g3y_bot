/**
 * claim 房间 策略
 */

let pro = {
    exec:function () {
        if(Game.time%3!=0)return;
        _.values(Game.flags).filter(e => e.getPrefix() == "claim").forEach(flag=>{
            if (Game.rooms[flag.pos.roomName]&&Game.rooms[flag.pos.roomName].my&&Game.rooms[flag.pos.roomName].spawn.length>0) {
                flag.remove()
            }
            if(!Memory.rooms[flag.pos.roomName]||!Memory.rooms[flag.pos.roomName][StationSources.stationName]){
                let spawnRoom = StationHive.getClosestSpawnRoom(flag.pos.roomName,7,4,15)
                if(!spawnRoom){
                    log("no active able room");
                    return;
                }
                if(Game.rooms[flag.getRoomName()]&&Game.rooms[flag.getRoomName()].my)spawnRoom=Game.rooms[flag.getRoomName()]
                let scouter = spawnRoom.creeps("scouter",false).filter(e=>e.headTask().roomName == flag.pos.roomName).head();
                if(!scouter){
                    let tasks = [UtilsTask.taskOutView(flag.id,flag.pos.roomName,undefined,undefined,"scouterToRoom")]
                    StationHive.trySpawn(spawnRoom,spawnRoom.name,[MOVE],"scouter",tasks)
                }
            }else {
                if(!Memory.rooms[flag.pos.roomName].structMap){ // 创建蓝图
                    ManagerAutoPlanner.computeRoom(flag);
                }else {
                    if(Game.rooms[flag.pos.roomName]&&Game.rooms[flag.pos.roomName].my)return;
                    let spawnRoom = StationHive.getClosestSpawnRoom(flag.pos.roomName,7,4,15)
                    if(!spawnRoom){
                        log("no active able room");
                        return;
                    }
                    if(Game.rooms[flag.getRoomName()]&&Game.rooms[flag.getRoomName()].my)spawnRoom=Game.rooms[flag.getRoomName()]
                    let controller = Memory.rooms[flag.pos.roomName][StationUpgrade.stationName][STRUCTURE_CONTROLLER]
                    let claimer = spawnRoom.creeps("claimer",false).filter(e=>e.headTask().roomName == flag.pos.roomName).head();
                    if(!claimer){
                        let tasks = [UtilsTask.taskOutView(controller.id,flag.pos.roomName,controller.x,controller.y,"claimRoom")]
                        StationHive.trySpawn(spawnRoom,spawnRoom.name,[CLAIM,MOVE,MOVE],"claimer",tasks)
                    }
                }

            }
        });
    }
}


global.StrategyClaim=pro;