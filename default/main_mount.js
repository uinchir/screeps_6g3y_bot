
// 数据结构
require("algo_wasm_PriorityQueue")
require("algo_UnionFind")

// class
require('class_RoomArray');

// helper
require('helper_visual');
require('helper_error');

// tools and addition
require("utils");
require("utils_task");
require("白名单v1.0");
require("超级移动优化hotfix 0.9.4");
require('极致建筑缓存 v1.4.3');
require('prototype_creep');
require('prototype_room');
require('prototype_roomPostiton');
require('prototype_store');
require('prototype_flag');




require('manager_rooms');
require('manager_creeps');
require('manager_flags');
require('manager_missions');
require('manager_crossShard');
require('manager_autoPlanner');
require('manager_planner');



require('station_defense');
require('station_sources');
require('station_minetral');
require('station_hive');
require('station_carry');
require('station_upgrade');
require('station_work');
require('station_tower');
require('station_lab');
require('station_observer');
require('station_factory');

require('strategy_lowLevel');
// require('strategy_middleLevel');
require('strategy_highLevel');
require('strategy_outerHarvest');
require('strategy_minSizeRoom');
require('strategy_claim');
require('strategy_atkL2');
require('strategy_scouter');
require('strategy_resourceBalance');
require('strategy_pillage');
require('strategy_market');
require('strategy_claimCrossShard');
require('strategy_deposits');
require('strategy_powerBank');


require('team_raL1');





