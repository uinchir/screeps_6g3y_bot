/**
 * 命名规则：
 * 前缀+房间+任务
 * 如果没房间使用 global 代替
 */




Flag.prototype.getRoomName = function () {
    let strLs = this.name.split("_");
    return strLs.length>=2?strLs[1]:undefined
}


Flag.prototype.getPrefix = function () {
    let strLs = this.name.split("_");
    return strLs.length>=1?strLs[0]:undefined
}



