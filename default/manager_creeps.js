
let pro={
    _time:0,
    // cacheCreeps:{},
    otherCreeps:[],
    // getRoomCreeps:function(roomName){
    //     return this.cacheCreeps[roomName]||[];
    // },
    init:function(){
        pro.otherCreeps = []

        for(let name in Game.creeps){
            if(!Memory.creeps[name]||!Memory.creeps[name].tasks)delete Game.creeps[name];//先删掉，之后跨shard要做处理
        }

        let creeps={};
        for (let name in Memory.creeps) {
            // if (Game.creeps[name]) Game.creeps[name].memory.dontPullMe = false;
            if (!Game.creeps[name]) {
                delete Memory.creeps[name];
            }else {
                let roomName = Memory.creeps[name].roomName;
                if(!name.startsWith("!")&&roomName){
                    creeps[roomName]=creeps[roomName]||[];
                    creeps[roomName].push(Game.creeps[name])
                }else{
                    creeps['global']=creeps['global']||[];
                    creeps['global'].push(Game.creeps[name])
                }
            }
        }


        _.keys(creeps).forEach(e=>{if(e!='global'){ Game.rooms[e]&&Game.rooms[e].setCreepsList(creeps[e]) }else{ pro.otherCreeps=creeps[e]}})
        // this.cacheCreeps = creeps ;
        // return creeps;
    },
    calcBodyPart:function(bodySet) {
        // 把身体配置项拓展成如下形式的二维数组
        // [ [ TOUGH ], [ WORK, WORK ], [ MOVE, MOVE, MOVE ] ]
        const bodys = Object.keys(bodySet).map(type => Array(bodySet[type]).fill(type));
        // 把二维数组展平
        return [].concat(...bodys)
    },
    getBodyEnergyNeed:function(body){
        let need=0;
        body.forEach(e=>{if(BODYPART_COST[e])need+=BODYPART_COST[e]});
        return need;
    },

};


global.ManagerCreeps = pro;