//-----a sample of whiteList------------
let whiteList = new Set([
    '6g3y',
    '6y3g',
    'BBleae',
    'BoosterKevin'
]);


/**
 Module: prototype.Whitelist
 Author: Yuandiaodiaodiao
 Date:   20200119
 Import:  require('prototype.Whitelist')
 Usage:
 1.write your whiteList in a Set
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
 2. return your whiteList in function getWhiteList
 3. all find method including Room.find RoomPosition.findClostestByRange/.findInRange/.findClostestByPath and so on
 with param type=== FIND_HOSTILE_CONSTRUCTION_SITES
 FIND_HOSTILE_POWER_CREEPS
 FIND_HOSTILE_CREEPS
 FIND_HOSTILE_SPAWNS
 FIND_HOSTILE_STRUCTURES
 now wont include objects which have owner in your whiteList
 4.use lookForAt / lookForAtArea / LookFor and give the first arg with
 "LOOK_FRIEND"   !!!notice its a string
 will return creep in whiteList (not include yourself)
 "LOOK_HOSTILE"   !!!notice its a string
 will return creep not in whiteList (also not include yourself)

 5.example:
 Game.rooms['E25N43'].lookForAt('LOOK_HOSTILE',25,25)
 Game.rooms['E25N43'].find(FIND_HOSTILE_CREEPS)

 */


function getWhitelist() {
    //----return your whiteList here-------------------
    return whiteList
}

//------module code------------

let originFind = Room.prototype.find
Room.prototype.find = function (type, opts) {
    let result = originFind.call(this, type, opts)
    if (type === FIND_HOSTILE_CREEPS
        || type === FIND_HOSTILE_CONSTRUCTION_SITES
        || type === FIND_HOSTILE_POWER_CREEPS
        || type === FIND_HOSTILE_SPAWNS
        || type === FIND_HOSTILE_STRUCTURES) {
        result = _.filter(result, o => !getWhitelist().has(o.owner.username))
    }
    return result
}
let originLookForAt = Room.prototype.lookForAt

function isFriend(o) {
    return getWhitelist().has(o.owner.username) && !o.my
}

function isHostile(o) {
    return !getWhitelist().has(o.owner.username) && !o.my
}

Room.prototype.lookForAt = function (type, firstArg, secondArg) {
    if (type === 'LOOK_FRIEND') {
        let result = originLookForAt.call(this, LOOK_CREEPS, firstArg, secondArg)
        result = _.filter(result, isFriend)
        return result
    } else if (type === 'LOOK_HOSTILE') {
        let result = originLookForAt.call(this, LOOK_CREEPS, firstArg, secondArg)
        result = _.filter(result, isHostile)
        return result
    } else {
        return originLookForAt.call(this, type, firstArg, secondArg)
    }
}
let originLookForAtArea = Room.prototype.lookForAtArea

function solveArea(result, asArray, o) {
    if (!asArray) {
        for (let i in result) {
            let temp = result[i]
            for (let j in temp) {
                let tmp = temp[j]
                if (tmp) {
                    tmp = _.filter(tmp, o => getWhitelist().has(o.owner.username) && !o.my)
                }
                if (tmp.length === 0) {
                    temp[i] = undefined
                } else {
                    temp[i] = tmp
                }
            }
        }
    } else {
        result = _.filter(result, o => getWhitelist().has(o.creep.owner.username) && !o.creep.my)
    }
    return result
}

Room.prototype.lookForAtArea = function (type, top, left, bottom, right, asArray) {
    if (type === 'LOOK_FRIEND') {
        let result = originLookForAtArea.call(this, LOOK_CREEPS, top, left, bottom, right, asArray)
        result = solveArea(result, asArray, isFriend)
        return result
    } else if (type === 'LOOK_HOSTILE') {
        let result = originLookForAtArea.call(this, LOOK_CREEPS, top, left, bottom, right, asArray)
        result = solveArea(result, asArray, isHostile)
        return result
    } else {
        return originLookForAtArea.call(this, type, top, left, bottom, right, asArray)
    }
}


















/**
 * 任务事件的 creep 通用的动作
 */


Creep.prototype.sayHeadTask=function () {
    let head = this.memory.tasks.head();
    if (head) {
        HelperVisual.showText(this,head.taskName)
        // this.say(head.taskName)
    }
};
Creep.prototype.sayLastTask=function () {
    let head = this.memory.tasks.last();
    if (head) {
        HelperVisual.showText(this,head.taskName)
        // this.say(head.taskName)
    }
};

Creep.prototype.execRegFun=function () {
    for(let task of this.memory.tasks){
        if (task&&task.regFun) {
            // log(head.regFun)
            this[task.regFun]();
        }

    }
    // let head = this.memory.tasks.head();
    // if (head&&head.regFun) {
    //     // log(head.regFun)
    //     this[head.regFun]();
    // }
};

Creep.prototype.popTask=function () {
    this.memory.tasks.pop()
    return this;
};

Creep.prototype.lastTask=function () {
    return this.memory.tasks.last()
};
Creep.prototype.headTask=function () {
    return this.memory.tasks.head()
};

Creep.prototype.lastTaskObj=function () {
    let last = this.memory.tasks.last();
    if(last) return Game.getObjectById(last.id)
};
Creep.prototype.headTaskObj=function () {
    let head = this.memory.tasks.head();
    if(head) return Game.getObjectById(head.id)
};

Creep.prototype.headTaskFlag=function () {
    let head = this.memory.tasks.head();
    if(head) return Game.flags[head.id];
};

Creep.prototype.headTask=function () {
    return this.memory.tasks.head()
};

Creep.prototype.addTask=function (task) {
    // log(this.memory.tasks.push(task))
    this.memory.tasks = this.memory.tasks.concat(task);
    return this;
};

Creep.prototype.addTaskAndExec=function (task) {
    this.memory.tasks = this.memory.tasks.concat(task);
    this.execLastTask();
    return this;
};

Creep.prototype.execLastTask=function () {
    if(this.memory.tasks.head()){
        this[this.memory.tasks.last().taskName]();
    }else {
        this.memory.dontPullMe = false;
    }
    return this;
};

Creep.prototype.isFree=function () {
    // return this.memory.tasks.head()==undefined; 任务数量比较快
    return !this.memory.tasks.length;
};

Creep.prototype.getPartCnt=function (bodyPart) {
    if(this.memory[bodyPart+"+"]==undefined){
        this.memory[bodyPart+"+"]=this.body.filter(e=>e.type==bodyPart).length;
    }
    return this.memory[bodyPart+"+"];
};

Creep.prototype.getUnBoostPartCnt=function (bodyPart) {
    return this.body.filter(e=>e.type==bodyPart&& !e.boost).length;
};


// Creep.prototype.getCarryAble=function (bodyPart) {
//     this.getPartCnt(CARRY)*50
// };



/**
 * 默认走到最后一个任务上
 * @param obj
 */
Creep.prototype.goTo=function (obj) {
    if(!obj)obj=this.goTo(this.memory.tasks.last());
    if(obj.pos)
        this.moveTo(obj.pos, {visualizePathStyle: {stroke: '#ff0374'}});
    else
        this.moveTo(new RoomPosition(obj.x,obj.y,obj.roomName), {visualizePathStyle: {stroke: '#ff0374'}})
};


Creep.prototype.scouterToRoom = function () {
    let obj=this.memory.tasks.last();
    let position = new RoomPosition(25,25,obj.roomName);
    if(this.room.name == obj.roomName){
        this.suicide();//自杀！
    }
    if(!Memory.rooms[this.room.name]||!Memory.rooms[this.room.name][StationSources.stationName]){
        ManagerRooms.updateRoom(this.room)
    }
    else this.goTo(position,{visualizePathStyle: {stroke: '#67ffed'}})
};

Creep.prototype.claimRoom = function () {
    let obj=this.lastTask();
    if(!obj.roomName&&!obj.pos){
        obj = Game.flags[obj.id];
    }
    if(!obj)return;
    if(this.room.name == (obj.roomName||obj.pos.roomName)){
        if(!this.room.my){
            if (this.claimController(this.room.controller) == ERR_NOT_IN_RANGE) {
                this.goTo(this.room.controller)
            }
        }
    }else{
        this.goTo(obj)
    }
};

/**
 * 默认走到最后一个任务上
 * @param obj
 */
Creep.prototype.goToPop=function () {
    let obj=this.memory.tasks.last();
    let position = new RoomPosition(obj.x,obj.y,obj.roomName);
    if(this.pos.isEqualTo(position)){
        this.popTask();
        this.execLastTask();
    }
    else this.goTo(position,{visualizePathStyle: {stroke: '#67ffed'}})
};
/**
 * 默认走到最后一个任务上
 * @param obj
 */
Creep.prototype.goToNearPop=function () {
    let obj=this.memory.tasks.last();
    let position = new RoomPosition(obj.x,obj.y,obj.roomName);
    if(this.pos.isNearTo(position)){
        this.memory.dontPullMe = true;
        this.popTask();
        this.execLastTask();
    }
    else this.moveTo(position,{visualizePathStyle: {stroke: '#67ffed'}})
};

Creep.prototype.storeEmpty=function () {
    return this.store.getUsedCapacity()==0;
};

/**
 * 爬 除了能量还有别的资源 （空的也不算）
 * @return {boolean}
 */
Creep.prototype.storeContainsEnergyOtherResType=function () {
    let resLs = _.keys(this.store)
    if(resLs.length>1)return true;
    if(resLs.length==0)return false;
    return resLs[0] != RESOURCE_ENERGY
};


Creep.prototype.storeUsed=function () {
    // return _.sum(_.values(this.store)) ==  this.store.getCapacity(RESOURCE_ENERGY);
    return  this.store.getUsedCapacity();
};
Creep.prototype.storeFull=function () {
    // return _.sum(_.values(this.store)) ==  this.store.getCapacity(RESOURCE_ENERGY);
    return  this.store.getFreeCapacity(RESOURCE_ENERGY)==0;
};

Creep.prototype.freeCapacity=function () {
    // return _.sum(_.values(this.store)) ==  this.store.getCapacity(RESOURCE_ENERGY);
    return  this.store.getFreeCapacity(RESOURCE_ENERGY);
};

Creep.prototype.mainRoom=function () {
    return Game.rooms[this.memory.roomName];
};


Creep.prototype.registerUsed=function () {
    let room = Game.rooms[this.memory["roomName"]]
    room.used = room.used||{}
    room.used[this.headTask().id] = true
};


Creep.prototype.pickupRes=function () {
    let target = this.lastTaskObj();
    if (!target) {
        this.popTask();
        return
    }
    let code = this.pickup(target);
    if(code==ERR_NOT_IN_RANGE){
        this.addTaskAndExec(UtilsTask.task(target,"goToNearPop"));
        this.execLastTask();
    }
    if (code != ERR_NOT_IN_RANGE) {
        this.popTask();
        this.execLastTask();
    }
};



/**
 * Task:
 * resType
 * resCount
 */
Creep.prototype.carryRes=function () {
    let task=this.lastTask();
    let obj = Game.getObjectById(task.id)
    if(!this.pos.isNearTo(obj)){
        this.moveTo(task,{visualizePathStyle: {stroke: '#67ffed'}})
    }
    if(!obj||obj.store[task.resType]==0||this.storeFull()){
        this.popTask();
        this.execLastTask();
        return;
    }
    if(this._move_res_active)return;
    this._move_res_active=true
    if(task.resType==undefined)throw new Error("resType no found:"+task)

    let code = this.withdraw(obj,task.resType,Math.min(task.resCount?Math.min(obj.store[task.resType]||0,this.store.getFreeCapacity(task.resType),task.resCount):undefined));
    // if(code==ERR_NOT_IN_RANGE){
    //  this.moveTo(task,{visualizePathStyle: {stroke: '#67ffed'}})
    // }
    if (code == OK) {
        this._move_res_active_OK = true
        let number = Math.min(Math.min(this.store.getFreeCapacity(task.resType),task.resCount? task.resCount:1e5),obj.store[task.resType])
        this.store[task.resType] = (this.store[task.resType]||0) + number
        obj.store[task.resType]-=number
        this.popTask();
        this.execLastTask();
    }else if(code==ERR_NOT_ENOUGH_RESOURCES){
        this.popTask();
    }

}

/**
 * Task:
 * resType 资源类型
 * resCount 资源数量
 * fromStorage 如果空了，往storage拿，默认true
 */
Creep.prototype.fillRes=function () {
    let task=this.lastTask();
    if(task.resType==undefined)throw new Error("resType no found:"+task)
    let obj = this.lastTaskObj();

    if(this[task.id])return this.popTask();
    if(!obj){
        this[task.id] = true;
        if(this.room.name==task.roomName) return this.popTask().execLastTask();
        else return;
    }
    // if((!this._move_res_active_OK)&&this.storeEmpty()&&this.room.my&&this.room.storage&&obj&&this.room.storage.id!=obj.id&&this.room.storage.store[task.resType]>=(task.resCount||this.store.getFreeCapacity(RESOURCE_ENERGY))&&task.fromStorage!==false){ // 如果满了,找自己的箱子拿东西
    //     this.addTask([UtilsTask.task(this.room.storage, "carryRes", undefined, {resType: task.resType,resCount:task.resCount})])
    //     this.execLastTask();
    //     return ;
    // }
    let carryAbleCnt = task.resCount || this.store.getFreeCapacity(RESOURCE_ENERGY);
    if((!this._move_res_active_OK)&&this.storeEmpty()&&this.room.my&&this.room.storage&&obj&&!StationCarry.isRoomMassStore(this.room,obj)&&StationCarry.roomMassStoreCnt(this.room,task.resType)>=carryAbleCnt&&task.fromStorage!==false){ // 如果满了,找自己的箱子拿东西
        let tasks = StationCarry.generatorMassStoreCarry(this.room,task.resType,carryAbleCnt)
        // log(this.room.name,task.resType,StationCarry.roomMassStoreCnt(this.room,task.resType),tasks.length)
        this.addTask(tasks)
        this.execLastTask();
        return ;
    }
    let objFreeCapacity = obj.store.getFreeCapacity(task.resType);
    if (obj&&objFreeCapacity==0||this.store[task.resType]==0) {
        this[task.id] = true;
        // log(obj.pos.roomName)
        this.popTask().execLastTask();
        return;
    }

    if(this.room.name!=task.roomName||!this.pos.isNearTo(obj)){
        this.moveTo(task,{visualizePathStyle: {stroke: '#67ffed'}})
        this._move_res_active=true
    }
    if(this._move_res_active)return;
    if(this.room.name==task.roomName&&this.pos.isNearTo(obj)){
        let number = Math.min(this.store[task.resType],task.resCount?task.resCount:100000);
        number = Math.min(objFreeCapacity,number);
        let code = this.transfer(obj,task.resType,number);
        if(code==OK&&task.resCount>number&&objFreeCapacity!=number){
            this._move_res_active=true
            task.resCount-=number;
            this.store[task.resType]-=number
            obj.store[task.resType] = (obj.store[task.resType]||0) + number
            return this.execLastTask();
        }
        if(code==OK){
            this._move_res_active=true
            this.store[task.resType]-=number
            obj.store[task.resType] = (obj.store[task.resType]||0) + number
            return this.popTask().execLastTask();
        }
    }
}

Creep.prototype.buildConst=function () {
    let obj=this.lastTaskObj();
    let code = this.build(obj);
    if(code == ERR_NOT_IN_RANGE) {
        this.moveTo(obj, {visualizePathStyle: {stroke: '#00f3ff',ignoreCreeps:false}});
    }
    if(!obj||this.storeEmpty()){
        this.popTask()
        this.execLastTask()
    }
}

Creep.prototype.fillAllTask = function () {
    let lastTaskObj = this.lastTaskObj();
    if(lastTaskObj&&this.store.getResTypeList().length){
        this.fillAll(lastTaskObj)
        this.execLastTask();
    }else{
        this.popTask().execLastTask();
    }
}


Creep.prototype.fillAllMainRoomStorage=function () {
    let storage = this.mainRoom().storage;
    if(storage){
        this.fillAll(storage)
    }
}

Creep.prototype.fillAll = function (target) {
    let tasks = _.keys(this.store).map(e=>
        UtilsTask.task(target,"fillRes",undefined,{
            resType: e
        })
    );
    this.addTask(tasks)
    if(tasks.length)this.execLastTask();
}



Creep.prototype.carryAllTask = function () {
    let lastTaskObj = this.lastTaskObj();
    if(lastTaskObj&&lastTaskObj.store.getResTypeList().length&&!this.storeFull()){
        this.carryAll(lastTaskObj,this.lastTask().regFun)
        this.execLastTask();
    }else{
        this.popTask().execLastTask();
    }
}

Creep.prototype.carryAll = function (target,register = "registerStationCarryDrop") {
    if(target)
    this.addTask(_.keys(target.store).map(e=>
        UtilsTask.task(target,"carryRes",register,{
            resType: e
        })
    ))
}

Creep.prototype.suicideTask = function () {
    this.addTask([UtilsTask.taskData("suicide")])
}


Creep.prototype.recycleCreep=function () {
    let task= this.headTask();
    if(this.mainRoom().name!=this.room.name){
        this.goTo(this.mainRoom().storage||this.mainRoom().controller);
    }else{
        let spawn=Game.getObjectById(task.id);
        if(!spawn||spawn.structureType!=STRUCTURE_SPAWN){
            spawn = this.mainRoom().spawn.head()
            if(spawn) task.id = spawn.id
        }
        if(spawn){
            this.say("recycle")
            if(!this.pos.isNearTo(spawn)){
                this.moveTo(spawn)
            }else {
                spawn.recycleCreep(this)
            }
        }
    }
};