/**
 * 整个房间杂物由他管理
 */



let pro={
    stationName:"stationObserver",
    ObserveRoomQueue:{},// id:room
    getClosedMyRoomName:function (roomName){
        Memory.rooms[roomName][pro.stationName] = Memory.rooms[roomName][pro.stationName]||{}
        let closedMyRoom = Memory.rooms[roomName][pro.stationName].closedMyRoom;
        if(closedMyRoom){
            if(Game.rooms[closedMyRoom]&&Game.rooms[closedMyRoom]&&Game.rooms[closedMyRoom].observer){
                return closedMyRoom;
            }
        }
        let visited = {[roomName]:1};
        let currentList = [roomName];
        for(let i=0;i<5;i++){// 计算5个联通房间内的房子
            let tmpList = []
            for(let rn of currentList){
                let nextNames = _.values(Game.map.describeExits(rn)).filter(e=>!visited[e])
                for(let nn of nextNames){
                    tmpList.push(nn)
                    visited[nn]=true;
                    if(Game.rooms[nn]&&Game.rooms[nn]&&Game.rooms[nn].observer){
                        Memory.rooms[roomName][pro.stationName].closedMyRoom = nn
                        return nn;
                    }
                }
            }
            currentList = tmpList;
        }
        return undefined;
    },
    getNearRoom:function(room){
        // Game.map.describeExits();
        let allRoom = []
        let visited = {[room.name]:1};
        let currentList = [room.name];
        for(let i=0;i<5;i++){// 计算5个联通房间内的房子
            let tmpList = []
            for(let rn of currentList){
                let nextNames = _.values(Game.map.describeExits(rn)).filter(e=>!visited[e])
                for(let nn of nextNames){
                    tmpList.push(nn)
                    allRoom.push(nn)
                    visited[nn]=true
                }
            }
            currentList = tmpList;
        }
        return allRoom;
    },
    observeLastRoom:function (room){
        let sm=room.memory[pro.stationName]=room.memory[pro.stationName]||{};
        sm.lastUpdateTime= Game.time
        let deposits = room.find(FIND_DEPOSITS);
        let powerBanks = room.find(FIND_STRUCTURES,{filter:e=>e.structureType==STRUCTURE_POWER_BANK});

        deposits.map(e=>{return {id:e.id,x:e.pos.x,y:e.pos.y,disappearTime:Game.time+e.ticksToDecay,lastCooldown:e.lastCooldown}})
            .forEach(data=> StrategyDeposits.createOrUpdateDepositMission(room.name,data));
        powerBanks.map(e=>{return {id:e.id,x:e.pos.x,y:e.pos.y,disappearTime:Game.time+e.ticksToDecay,power:e.power}})
            .forEach(data=> StrategyPowerBank.createOrUpdatePowerBankMission(room.name,data));

        // sm.powerBanks = powerBanks.map(e=>{return {x:e.pos.x,y:e.pos.y,disappearTime:Game.time+e.ticksToDecay}});
        // log(room.name,sm.powerBanks)
        // log(room.name,sm.deposits )
    },
    obOverRooms:function (room){
        if(!room.observer||!room.memory[pro.stationName])return;// 如果没有ob就不动
        let lastRoomName = room.memory[pro.stationName].lastRoomName
        if(lastRoomName&&Game.rooms[lastRoomName]){
            // log(room.name,Game.time,pro.ObserveRoomQueue[room.observer.id])
            pro.observeLastRoom(Game.rooms[lastRoomName]);
            delete room.memory[pro.stationName].lastRoomName;
        }
        if(room.observer&&pro.ObserveRoomQueue[room.observer.id]&&pro.ObserveRoomQueue[room.observer.id].head()){
            let roomName = pro.ObserveRoomQueue[room.observer.id].shift();
            room.observer.observeRoom(roomName);
            room.memory[pro.stationName].lastRoomName=roomName;
        }
    },
    update:function (room) {
        if(!room.observer)return;// 如果没有ob就不动
        // if(room.name=="W1N4")log(pro.getNearRoom(room))
        let checkTimeDelay = 31*7;//checkTimeDelay tick 更新一次
        if((Game.time+room.hashCode())%(checkTimeDelay)!=0)return;
        let sm=room.memory[pro.stationName]=room.memory[pro.stationName]||{};
        sm.roomNames = pro.getNearRoom(room)

        sm.roomNames = sm.roomNames||[]
        let overRoomNames = sm.roomNames.filter(e=>e.indexOf("0")>0)// 过道
        pro.ObserveRoomQueue[room.observer.id] = []
        for(let rn of overRoomNames){
            // sm.lastCheckTime = Game.time;
            if(!Memory.rooms[rn])Memory.rooms[rn] = {}
            let rm=Memory.rooms[rn][pro.stationName]=Memory.rooms[rn][pro.stationName]||{};
            let lastUpdate = rm.lastUpdateTime||0
            if(Game.time - lastUpdate>checkTimeDelay/2){
                pro.ObserveRoomQueue[room.observer.id].push(rn)
            }
        }

    },
};



global.StationObserver=pro;