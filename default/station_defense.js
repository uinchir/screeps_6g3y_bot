/**
 * 整个房间杂物由他管理
 */





Creep.prototype.repairWall = function () {

    if (this.store[RESOURCE_ENERGY] == 0) {
        this.popTask()
    }
    let obj = this.lastTaskObj();
    let code = this.repair(obj);
    if(!obj||obj.hits>=obj.hitsMax){
        this.popTask();
    }
    if (code == ERR_NOT_IN_RANGE) {
        this.moveTo(obj, {visualizePathStyle: {stroke: '#fffa00'}});
    }
    if(this.ticksToLive%3){
        this.memory.dontPullMe = false;
    }
}


let pro={
    stationName:"stationDefense",
    levelWallHits:{
        1:5000,2:5000,3:5000,4:50000,5:500000,6:1000000,7:3000000,8:10*1000000 // 8级最少修10m墙壁
    },
    // wallListMap:{},
    needBuildWall:function (room){
        if(!pro.needRepairWallMap[room.name])return false;
        let wall = Game.getObjectById(pro.needRepairWallMap[room.name].head());
        return wall&&wall.hits<pro.levelWallHits[room.level];
    },
    needBuildWallWorkerFree:function (room){
        if(!pro.needRepairWallMap[room.name])return false;
        let wall = Game.getObjectById(pro.needRepairWallMap[room.name].head());
        return wall&&wall.hits<299*1000000;
    },
    generatorRepairTask(room){
        if(!pro.needRepairWallMap[room.name])return [];
        let wall = undefined;
        while(pro.needRepairWallMap[room.name].length&&!wall){
            wall = Game.getObjectById(pro.needRepairWallMap[room.name].shift())
        }
        if(wall) return [ UtilsTask.task(wall,"repairWall","registerUsed")]
        return []
    },
    needRepairWallMap:{},
    update:function (room) {
        // delete room.memory[pro.stationName]
        // delete room.memory[undefined]
        let tmp = {};

        if (!room.memory.structMap) return;
        if (!room.storage) return;

        let created = false
        let hasConSite = room.find(FIND_CONSTRUCTION_SITES).length
        for(let structs of [STRUCTURE_WALL,STRUCTURE_RAMPART]){
            if(room.memory.structMap[structs]){
                tmp[structs] = Utils.decodePosArray(room.memory.structMap[structs]).map(e=>{
                    let pos = new RoomPosition(e.x,e.y,room.name);
                    let wall = pos.lookFor(LOOK_STRUCTURES).filter(e=>e.structureType==structs).head();
                    if(!wall&&!hasConSite &&!created) {
                        if (OK == pos.createConstructionSite(structs))
                            created = true
                    }
                    if(wall)return wall
                }).filter(e=>e)
            }
        }
        let rampartCover = [STRUCTURE_SPAWN,STRUCTURE_TERMINAL,STRUCTURE_STORAGE,STRUCTURE_TOWER,STRUCTURE_POWER_SPAWN,STRUCTURE_FACTORY,STRUCTURE_NUKER]
        let coverMap = {}
        for(let i = 0;i<rampartCover.length;i++){coverMap[rampartCover[i]] = i+1;}
        if(!tmp[STRUCTURE_RAMPART])tmp[STRUCTURE_RAMPART]=[]
        // 8级后开始给东西上镀金
        if(room.level==8){
            tmp[STRUCTURE_RAMPART] = tmp[STRUCTURE_RAMPART].concat(room.find(FIND_STRUCTURES,{filter:e=>coverMap[e.structureType]})
                .sort((a,b)=>coverMap[a.structureType]-coverMap[b.structureType])
                .map(e=>{
                    // HelperVisual.showText(e,coverMap[e.structureType])
                    let rampart = e.pos.lookFor(LOOK_STRUCTURES).filter(e=>e.structureType == STRUCTURE_RAMPART).head();
                    if(!rampart&&!hasConSite &&!created) {
                        if (OK == e.pos.createConstructionSite(STRUCTURE_RAMPART))
                            created = true
                }
                return rampart
            }).filter(e=>e))
        }

        tmp = tmp[STRUCTURE_RAMPART].concat(tmp[STRUCTURE_WALL]).filter(e=>e).sort((a, b) => a.hits - b.hits).map(e=>e.id)

        pro.needRepairWallMap[room.name] = tmp;

    },
    checkSafeMode:function (room){
        if(room.name!="E9N24")return;
        // let hostileCnt = room.find(FIND_HOSTILE_CREEPS,{filter:e => e.owner.username != "Invader" && e.body.filter(e=>e.type==HEAL).length >= 5 }).length;
        // if(!hostileCnt)return;
        // room.controller.pos.createFlag("raL1_W19N21_crossShard_114514")
        // let MyRuinCnt = room.find(FIND_RUINS,{filter:e=>e.structure.owner&&e.structure.owner.username==room.controller.owner.username
        //         &&e.structure.structureType!=STRUCTURE_ROAD
        //         &&e.structure.structureType!=STRUCTURE_CONTAINER
        //         &&e.structure.structureType!=STRUCTURE_RAMPART
        //         &&e.structure.structureType!=STRUCTURE_EXTRACTOR
        //         &&e.structure.structureType!=STRUCTURE_LINK}).length
        // if(!MyRuinCnt)return;
        // room.controller.activateSafeMode()
    }
};



global.StationDefense=pro;