
global.STORE_IN=0;
global.STORE_CACHE=1;
global.STORE_OUT=2;//只存储能量


Creep.prototype.registerStationCarryDrop=function () {
    let room = Game.rooms[this.memory["roomName"]]
    room.dropUsed = room.dropUsed||{}
    let id = this.headTask().id;
    room.dropUsed[id] = true
};


let pro={
    stationName:"stationCarry",
    isRoomMassStore:function (room,obj){
        if(room.storage&&room.storage.id==obj.id) return true
        if(room.terminal&&room.terminal.id==obj.id) return true
        // if(room.factory&&room.factory.id==obj.id) return true
        return false;
    },
    roomMassStoreCnt:function (room,resType){
        let cnt = 0;
        if(room.storage) cnt += room.storage.store.getUsedCapacity(resType);
        if(room.terminal) cnt += room.terminal.store.getUsedCapacity(resType);
        // if(room.factory) cnt += room.factory.store.getUsedCapacity(resType);
        return cnt;
    },
    generatorMassStoreCarry:function (room,resType,resCount){
        let tasks = []
        for(let target of [room.storage,room.terminal]){//,room.factory
            if(!target)continue;
            let currentCnt = Math.min(target.store[resType] || 0,resCount);
            resCount-=currentCnt
            if(currentCnt)tasks.push(UtilsTask.task(target, "carryRes", undefined, {resType: resType,resCount:currentCnt}))
            if(resCount<0)break;
        }
        return tasks;
    },
    getCarrierBodyConfig:function (room){
        let totalEnergy = room.getEnergyCapacityAvailable(room);
        let body = [CARRY,CARRY,MOVE];
        let bodyEnergy = Utils.getBodyEnergyNeed(body)
        let num = 0
        for(let i=1;i*bodyEnergy<=totalEnergy;i++){
            if(num>=17)break;
            num+=1
        }
        return ManagerCreeps.calcBodyPart({  [CARRY]: num<17?num*2:num*2-1 ,[MOVE]: num });//
    },
    generatorCarryStorageEnergyTask:function (room,energyCnt=6600){
        if(room.storage.store[RESOURCE_ENERGY]>2000){
            return [UtilsTask.task(room.storage,"carryRes",undefined,{
                resType: RESOURCE_ENERGY,resCount:energyCnt
            })]
        }
        let resCnt = pro.roomMassStoreCnt(room,RESOURCE_ENERGY);
        if(resCnt>Math.max(energyCnt,2000)){
            return pro.generatorMassStoreCarry(room,RESOURCE_ENERGY,Math.min(energyCnt,resCnt))
        }
        return []
    },
    generatorPickTask:function(room,onlyEnergy=false){ // 可能有bug\
        // 拿全部的
        room.dropUsed = room.dropUsed||{}
        let tasks = room.find(FIND_TOMBSTONES).concat(room.find(FIND_RUINS))
            .filter(e=>!room.dropUsed[e.id])
            .map(drops => {
                if(drops.store.getUsedCapacity(onlyEnergy?RESOURCE_ENERGY:undefined)) {
                    if(onlyEnergy)
                        return [UtilsTask.task(drops,"carryRes","registerStationCarryDrop",{
                            resType: RESOURCE_ENERGY
                        })]
                    else
                        return _.keys(drops.store).map(e=>
                            UtilsTask.task(drops,"carryRes","registerStationCarryDrop",{
                                resType: e
                            })
                        )
                }
                return null
            }).filter(e=>e);
        return room.find(FIND_DROPPED_RESOURCES)
            .filter(e=>!room.dropUsed[e.id])
            .filter(e=>onlyEnergy?e.resourceType==RESOURCE_ENERGY:true).map(dropRes => {
                return [UtilsTask.task(dropRes,"pickupRes","registerStationCarryDrop",{
                    resType: dropRes.resourceType
                })]
            }).concat(tasks);
        // return
    },
    transformLink:function (room){
        if(!room.memory[StationUpgrade.stationName]||!room.memory[pro.stationName])return;
        let upgradeLink = Game.getObjectById(room.memory[StationUpgrade.stationName][STRUCTURE_LINK])
        let centerLink = Game.getObjectById(room.memory[pro.stationName][STRUCTURE_LINK])

        _.values(room.memory[StationSources.stationName]).forEach(e=>{
            let link = Game.getObjectById(e[STRUCTURE_LINK]);
            let container = Game.getObjectById(e[STRUCTURE_CONTAINER]);
            if(!link)return;
            if(link&&upgradeLink&&link.store.getFreeCapacity(RESOURCE_ENERGY) == 0 && upgradeLink.store.isEmpty()) {
                link.transferEnergy(upgradeLink);
                upgradeLink = null
                return;
            }

            if(link&&centerLink&&link.store.getFreeCapacity(RESOURCE_ENERGY) == 0 && centerLink.store.isEmpty()) {
                if(container&&container.store[RESOURCE_ENERGY]>800){
                    link.transferEnergy(centerLink);
                    centerLink = null
                    return;
                }
            }
        })

        if(upgradeLink && upgradeLink.store.isEmpty() && centerLink && !centerLink.store.isEmpty()){
            centerLink.transferEnergy(upgradeLink);
        }
        // room.
    },
    update:function (room) {
        let objs=room.memory[pro.stationName]||{};
        let centerLink = null;
        if(room.storage) room.link.forEach(e=>{
            if (e.pos.isNearTo(room.storage)) {
                centerLink = e
            }
        })

        if(centerLink){
            objs[STRUCTURE_LINK] = centerLink.id
        }

        room.memory[pro.stationName]=objs;
    },

    generatorFillNukerTask(room){
        room.used = room.used||{};
        if(room.storage&&room.nuker&&!room.used[room.nuker.id]){
            let gNeed = room.nuker.store.getFreeCapacity(RESOURCE_GHODIUM);
            if(gNeed>0&&room.storage.store[RESOURCE_GHODIUM]>=1650){
                let ops = {resType: RESOURCE_GHODIUM,resCount:gNeed}
                return [UtilsTask.task(room.nuker, "fillRes", "registerUsed",ops),
                    UtilsTask.task(room.storage, "carryRes", "registerUsed",ops)]
            }
            let enNeed = room.nuker.store.getFreeCapacity(RESOURCE_ENERGY);
            if(enNeed>0&&room.storage.store[RESOURCE_ENERGY]>50000){
                let ops = {resType: RESOURCE_ENERGY,resCount:enNeed}
                return [UtilsTask.task(room.nuker, "fillRes", "registerUsed",ops),
                    UtilsTask.task(room.storage, "carryRes", "registerUsed",ops)]
            }
        }
        return [];
    },
    generatorFillPowerSpawnTask(room){
        room.used = room.used||{};
        if(room.storage&&room.terminal&&room.powerSpawn&&!room.used[room.powerSpawn.id]){
            let powerNeed = room.powerSpawn.store.getFreeCapacity(RESOURCE_POWER);
            if(powerNeed==100&&room.terminal.store[RESOURCE_POWER]>=powerNeed){
                let ops = {resType: RESOURCE_POWER,resCount:powerNeed}
                return [UtilsTask.task(room.powerSpawn, "fillRes", "registerUsed",ops),
                    UtilsTask.task(room.terminal, "carryRes", "registerUsed",ops)]
            }
            if(powerNeed==100&&room.storage.store[RESOURCE_POWER]>=powerNeed){
                let ops = {resType: RESOURCE_POWER,resCount:powerNeed}
                return [UtilsTask.task(room.powerSpawn, "fillRes", "registerUsed",ops),
                    UtilsTask.task(room.storage, "carryRes", "registerUsed",ops)]
            }
            let enNeed = room.powerSpawn.store.getFreeCapacity(RESOURCE_ENERGY);
            if(enNeed>2000&&room.storage.store[RESOURCE_ENERGY]>115000){
                let ops = {resType: RESOURCE_ENERGY,resCount:enNeed}
                return [UtilsTask.task(room.powerSpawn, "fillRes", "registerUsed",ops),
                    UtilsTask.task(room.storage, "carryRes", "registerUsed",ops)]
            }
        }
        return [];
    },
};



global.StationCarry=pro;