/**
 * 外矿策略
 */

let pro = {
    exec:function (room) {
        if((Game.time+room.hashCode())%6!=0)return;
        if(!room.storage)return;
        room.flags("har").sort().forEach(flag=>{
            if (Memory.rooms[flag.pos.roomName]) {
                StationSources.trySpawnOuterDefenser(flag.pos.roomName,room);
            }
        })
        room.flags("har").sort().forEach(flag=>{
            if(!Memory.rooms[flag.pos.roomName]||!Memory.rooms[flag.pos.roomName][StationSources.stationName]){
                let scouter = room.creeps("scouter",false).filter(e=>e.headTask().roomName == flag.pos.roomName).head();
                // log(scouter.headTask().roomName)
                if(!scouter){
                    let tasks = [UtilsTask.taskOutView(flag.id,flag.pos.roomName,undefined,undefined,"scouterToRoom")]
                    StationHive.trySpawn(room,room.name,[MOVE],"scouter",tasks)
                }
            }
            else{
                let harRoom = Game.rooms[flag.pos.roomName];
                if((Game.time+room.hashCode())%30==0&&harRoom){
                    StationSources.update(harRoom)
                }
                if(harRoom){ // 先生claimer 再生 har 保证能量获取效率 没有视野会先生 har
                    let reserver = room.creeps("reserver",false).filter(e=>e.headTask().roomName == flag.pos.roomName).head();
                    if(!reserver&&(!harRoom.controller.reservation||harRoom.controller.reservation.ticksToEnd<1000)){
                        let tasks = [UtilsTask.task(harRoom.controller,"reserveOuterHar")]
                        let body = StationSources.getReverserBodyConfig(room.getEnergyCapacityAvailable())
                        StationHive.trySpawn(room,room.name,body,"reserver",tasks)
                    }
                }
                StationSources.trySpawnOuterHarKeeper(flag.pos.roomName,room);
                StationSources.trySpawnOuterHarCarrier(flag.pos.roomName,room);

            }
        })
    }
}


global.StrategyOuterHarvest=pro;