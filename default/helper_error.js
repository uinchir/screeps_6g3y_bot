

pro_err={
    lastTick:-1,
    errList:[],
    catchError:function (func){
        try{
            return func()
        }catch (e) {
            if(Game.time!=pro_err.lastTick)pro_err.errList = []
            let data = e.stack
            pro_err.errList.push(data+"\n\n**************\n")
        }
    },
    throwAllError:function () {
        if(pro_err.errList.length)throw new Error(pro_err.errList);
    },
}

global.HelperError=pro_err