/**
 * claimCrossShard 房间 策略
 * claimCrossShard_shard3_W19N21_1
 */

let pathData = {
    path: [
        { shard: 'shard3', roomName: 'W20N20', x: 13, y: 24 },
        { shard: 'shard2', roomName: 'W20N20', x: 13, y: 12 },
        { shard: 'shard1', roomName: 'W20N20', x: 38, y: 12 },
        { shard: 'shard0', roomName: 'W30N39', x: 40, y: 1 },
        { shard: 'shard0', roomName: 'W20N40', x: 24, y: 23 },
        { shard: 'shard1', roomName: 'W10N20', x: 14, y: 38 },
        { shard: 'shard0', roomName: 'W10N31', x: 37, y: 48 },
        { shard: 'shard0', roomName: 'E10N30', x: 41, y: 22 },
        { shard: 'shard1', roomName: 'E10N20', x: 25, y: 8 }
    ],
    distance: 269,
    totalRooms: 19
}


let pro = {
    exec:function () {
        if(Game.time%3!=0)return;
        _.values(Game.flags).filter(e => e.getPrefix() == "claimCrossShard").forEach(flag=>{
            if (Game.rooms[flag.pos.roomName]&&Game.rooms[flag.pos.roomName].my&&Game.rooms[flag.pos.roomName].spawn.length>0) {
                flag.remove()
            }
            if(!flag.memory.spawnTime)flag.memory.spawnTime = 0
            let room = Game.rooms[flag.pos.roomName]
            if(!room||!room.my){// 如果房间不是我的
                if(Game.time-flag.memory.spawnTime>300){
                    let split = flag.name.split("_");
                    let mission = {
                        func:"spawnCreepCressShard",
                        data: {
                            spawnRoom:split[2],
                            targetRoomName:flag.pos.roomName,
                            String,body:[MOVE,MOVE,CLAIM],
                            role:"claimer",
                            tasks:[
                                UtilsTask.taskFlag(flag,"claimRoom"),
                                UtilsTask.taskData("moveCrossShardByPath",undefined,pathData)
                            ],
                        }
                    }
                    flag.memory.spawnTime = Game.time
                    ManagerCrossShard.addCrossShardRequest(split[1],mission)
                }
            }else {
                if(!Memory.rooms[flag.pos.roomName].structMap){ // 创建蓝图
                    ManagerAutoPlanner.computeRoom(flag);
                }else {
                    if(Game.time-flag.memory.spawnTime>300){
                        let split = flag.name.split("_");
                        let mission = {
                            func:"spawnCreepCressShard",
                            data: {
                                spawnRoom:split[2],
                                targetRoomName:flag.pos.roomName,
                                String,body:ManagerCreeps.calcBodyPart({ [WORK]: 16, [CARRY]: 18, [MOVE]: 16 }),// 这里写死了
                                role:"worker",
                                tasks:[
                                    UtilsTask.taskData("moveCrossShardByPath",undefined,pathData)
                                ],
                            }
                        }
                        flag.memory.spawnTime = Game.time
                        ManagerCrossShard.addCrossShardRequest(split[1],mission)
                    }
                }
            }
        });
    }
}


global.StrategyClaimCrossShard=pro;