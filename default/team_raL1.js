/**
 * 一体机
 * raL1[_roomName][_keeper][_hash]
 * raL1_W1N4_1
 * raL1_W3N8_1
 * raL1_W23N19_1
 * raL1_W19N21_crossShard_1
 */

// let pathData = {
//     path: [
//         { shard: 'shard3', roomName: 'W20N20', x: 13, y: 24 },
//         { shard: 'shard2', roomName: 'W20N20', x: 13, y: 12 },
//         { shard: 'shard1', roomName: 'W20N20', x: 38, y: 12 },
//         { shard: 'shard0', roomName: 'W30N39', x: 40, y: 1 },
//         { shard: 'shard0', roomName: 'W20N40', x: 24, y: 23 },
//         { shard: 'shard1', roomName: 'W10N20', x: 14, y: 38 },
//         { shard: 'shard0', roomName: 'W10N31', x: 37, y: 48 },
//         { shard: 'shard0', roomName: 'E10N30', x: 41, y: 22 },
//         { shard: 'shard1', roomName: 'E10N20', x: 25, y: 8 }
//     ],
//     distance: 269,
//     totalRooms: 19
// }
let pathData = {
    path: [ { shard: 'shard3', roomName: 'W20N20', x: 13, y: 24 },
            { shard: 'shard2', roomName: 'W20N20', x: 13, y: 12 },
            { shard: 'shard1', roomName: 'W20N20', x: 38, y: 12 },
            { shard: 'shard0', roomName: 'W30N39', x: 40, y: 1 },
            { shard: 'shard0', roomName: 'W20N40', x: 24, y: 23 },
            { shard: 'shard1', roomName: 'W10N20', x: 14, y: 38 },
            { shard: 'shard0', roomName: 'W10N31', x: 37, y: 48 },
            { shard: 'shard0', roomName: 'E10N30', x: 41, y: 22 },
            { shard: 'shard1', roomName: 'E10N20', x: 29, y: 43 },
            { shard: 'shard0', roomName: 'E20N41', x: 44, y: 48 },
            { shard: 'shard0', roomName: 'E29N40', x: 48, y: 3 },
            { shard: 'shard0', roomName: 'E30N50', x: 43, y: 16 },
            { shard: 'shard1', roomName: 'E20N30', x: 32, y: 10 },
            { shard: 'shard0', roomName: 'E40N59', x: 29, y: 1 },
            { shard: 'shard0', roomName: 'E70N60', x: 12, y: 11 },
            { shard: 'shard1', roomName: 'E40N30', x: 18, y: 22 },
            { shard: 'shard0', roomName: 'E71N50', x: 1, y: 41 },
            { shard: 'shard0', roomName: 'E70N20', x: 30, y: 9 },
            { shard: 'shard1', roomName: 'E40N10', x: 23, y: 34 },
            { shard: 'shard0', roomName: 'E70N9', x: 19, y: 1 },
            { shard: 'shard0', roomName: 'E60N10', x: 35, y: 45 },
            { shard: 'shard1', roomName: 'E30N10', x: 35, y: 19 },
            { shard: 'shard0', roomName: 'E61N20', x: 1, y: 10 },
            { shard: 'shard0', roomName: 'E60N80', x: 32, y: 34 },
            { shard: 'shard1', roomName: 'E30N40', x: 8, y: 33 },
            { shard: 'shard2', roomName: 'E30N40', x: 13, y: 29 } ],
    distance: 536,
    totalRooms: 53
}

Creep.prototype.registerRaL1=function () {
    let flag= Game.flags[this.headTask().id];
    if(flag&&flag.memory)
        flag.memory.creepName = this.name
}

Creep.prototype.raL1=function () {
    let flag= Game.flags[this.headTask().id];
    // if(!flag)this.suicide();
    let inner=pos=> pos.x>=2&&pos.x<=48&&pos.y>=2&&pos.y<=48;
    this.atk=function(){
        let em=null//this.pos.findClosestByPath(FIND_HOSTILE_CREEPS);//
        let isHostileCreep = false;
        let isHostileConstruction = false;
        // if(!em)em=this.pos.findClosestByPath(FIND_HOSTILE_POWER_CREEPS);
        em=this.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES,{filter:e=>e.structureType==STRUCTURE_TOWER})
        if(!em)em=this.pos.findClosestByPath(FIND_HOSTILE_CREEPS,{filter:e=>e.body.filter(e=>e.type==ATTACK||e.type==RANGED_ATTACK)});
        if(em)isHostileCreep = true;
        if(!em){
            em=this.pos.findClosestByPath(FIND_HOSTILE_CONSTRUCTION_SITES,{filter:e=>e.structureType==STRUCTURE_SPAWN})
            if(em)isHostileConstruction = true
        }
        if(!em)em=this.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES,{filter:e=>e.structureType==STRUCTURE_SPAWN});
        if(!em)em=this.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES,{filter:e=>e.structureType==STRUCTURE_TOWER});
        if(!em)em=this.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES,{filter:e=>e.structureType!=STRUCTURE_INVADER_CORE&&e.structureType!=STRUCTURE_CONTROLLER&&e.structureType!=STRUCTURE_RAMPART});//&&e.structureType!=STRUCTURE_SPAWN
        if(!em)em=this.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES,{filter:e=>e.structureType!=STRUCTURE_INVADER_CORE&&e.structureType!=STRUCTURE_CONTROLLER});

        if(em)HelperVisual.showText(em,"X")
        if(isHostileConstruction){
            if(this.rangedAttack(em)==ERR_NOT_IN_RANGE){//||!inner(this.pos)
                this.rangedMassAttack()
            }
            this.moveTo(em)
        } else if(isHostileCreep){
            if(this.rangedAttack(em)==ERR_NOT_IN_RANGE){//||!inner(this.pos)
                this.rangedMassAttack()
            }
            this.moveTo(em);
            // if(this.pos.inRangeTo(em,2)){
            //     em.range = 4
            //     let path = PathFinder.search(this.pos,em,{flee:true}).path;
            //     let code = this.moveByPath(path)
            //     // log(code,PathFinder.search(this.pos,em,{flee:true}))
            // }
        }else if(em){
            if(this.rangedAttack(em)==ERR_NOT_IN_RANGE){//||!inner(this.pos)
                this.rangedMassAttack()
            }
            if(!this.pos.inRangeTo(em,1)){
                this.moveTo(em)
            }
        } else if(!em&&!this.pos.isEqualTo(tarPos)){
            this.moveTo(tarPos);
        }
        if(this.hits+350<this.hitsMax){
            let exit=this.pos.findClosestByPath(FIND_EXIT);
            if(exit)this.moveTo(exit);
        }
    };

    this.heal(this)
    if(!flag)return;
    let tarPos =flag.pos;
    if(this.room.name!=flag.pos.roomName) {
        let inner=pos=> pos.x>2&&pos.x<47&&pos.y>2&&pos.y<47;
        if(this.hits!=this.hitsMax&&!inner(this.pos)){
            this.moveTo(new RoomPosition(25,25,this.room.name));
            //let t=this.$moveTo(tarPos);
        }else if(this.hits==this.hitsMax){
            this.moveTo(tarPos);
        }else{
            this.atk();
        }
    }else{
        this.atk();
    }
};

let pro = {

    // raBody:ManagerCreeps.calcBodyPart({ [TOUGH]: 11, [RANGED_ATTACK]: 6, [MOVE]: 10 , [HEAL]: 23 }),
    // raBoost:{[BOOST_RES["damage"][2]]:30*11,[BOOST_RES["fatigue"][2]]:30*10,[BOOST_RES["rangedAttack"][2]]:30*6,[BOOST_RES["heal"][2]]:30*23},
    raBody:ManagerCreeps.calcBodyPart({ [TOUGH]: 11, [RANGED_ATTACK]: 6, [MOVE]: 10 , [HEAL]: 23 }),
    raBoost:{[BOOST_RES["damage"][2]]:30*11,[BOOST_RES["fatigue"][2]]:30*10,[BOOST_RES["rangedAttack"][2]]:30*6,[BOOST_RES["heal"][2]]:30*23},
    // raBody:ManagerCreeps.calcBodyPart({ [TOUGH]: 1, [RANGED_ATTACK]: 1, [MOVE]: 1 , [HEAL]: 1 , [CARRY]: 1 , [ATTACK]: 1, [WORK]: 1 }),
    // raBoost:{[BOOST_RES["damage"][0]]:30*1,[BOOST_RES["fatigue"][0]]:30*1,[BOOST_RES["rangedAttack"][0]]:30*1,[BOOST_RES["heal"][0]]:30*1,[BOOST_RES["capacity"][0]]:30*1,[BOOST_RES["attack"][0]]:30*1,[BOOST_RES["build"][0]]:30*1},
    execSpawnCrossShard:function (flag) {
        if(!flag.memory.taskId){
            let mission = {
                func:"spawnCreepCressShard",
                callback:"setFlagMemory",
                data: {
                    flagName:flag.name,
                    flagMemory:{
                        spawnTime:Game.time+pathData.distance+150
                    },
                    spawnRoom:flag.getRoomName(),
                    targetRoomName:flag.pos.roomName,
                    body:pro.raBody,
                    role:"raL1",
                    tasks:[
                        UtilsTask.taskFlag(flag,"raL1","registerRaL1"),
                        UtilsTask.taskData("moveCrossShardByPath",undefined,pathData),
                        UtilsTask.taskData("boostCreepBodyPart","registerBoostCreep",{data:pro.raBoost})
                    ],
                }
            }
            // flag.memory.spawnTime = Game.time
            flag.memory.taskId = ManagerCrossShard.addCrossShardRequest("shard3",mission)
        }

    },
    execSpawn:function (flag) {
        if(!flag.memory.creepName){
            let room = Game.rooms[flag.getRoomName()]
            if(!room||!room.my||room.level<8){
                flag.remove();console.log(flag.getRoomName()+" 不是你的房间或没8级");
                return;
            }
            // log(pro.raBoost)
            if(!StationLab.boostAble(room,pro.raBoost)){
                console.log(room.name+" 资源不足");
                return;
            }

            let task =  [
                UtilsTask.taskFlag(flag,"raL1","registerRaL1"),
                UtilsTask.taskData("moveCrossShardByPath",undefined,pathData),
                // UtilsTask.task(StationLab.getUnboostContainer(room),"UnboostCreepBodyPart","registerUnboostCreep"),
                UtilsTask.taskData("boostCreepBodyPart","registerBoostCreep",{data:pro.raBoost})
            ]
            StationHive.trySpawn(room,"global",pro.raBody,"raL1",task)
        }else if(!Game.creeps[flag.memory.creepName]){
            if(flag.name.indexOf("_keeper")>=0){
                delete flag.memory.creepName;
            }
            // else{
            //     flag.remove();
            // }
        }
    },
    exec:function () {
        if(Game.time%3!=0)return;
        _.values(Game.flags).filter(e => e.getPrefix() == "raL1").forEach(flag=>{
            if(flag.name.indexOf("crossShard")>0)
                pro.execSpawnCrossShard(flag)
            else
                pro.execSpawn(flag)
        });
    }
}

global.TeamRaL1=pro;