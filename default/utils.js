Array.prototype.flat= function(){return _.flatten(this)};
// Array.prototype.reduce= function(func){return _.reduce(this,func)};
Array.prototype.zip= function (another){return _.zip(this,another)};
Array.prototype.contains= function (another){return _.contains(this,another)};
Array.prototype.take= function (n){return _.take(this,n)};
Array.prototype.head= function(){return _.head(this)};
Array.prototype.last= function(){return _.last(this)};
Array.prototype.without= function(...e){return _.without(this,...e)};
Array.prototype.log= function(){console.log(JSON.stringify(this));return this};

clog=function(...e){console.log(...e)};
log=function(...e){console.log(JSON.stringify(e))};


let posCodeNumberMap = {};
let posCodeCharMap = {};
// pos 转换 char 用的
(function (){
    let a = 'a'.charCodeAt(0)
    let A = 'A'.charCodeAt(0)
    for(let i=0;i<25;i++){
        let b = String.fromCharCode(a+i)
        let j = 25+i
        let B = String.fromCharCode(A+i)
        posCodeNumberMap[i] = b
        posCodeCharMap[b] = i
        posCodeNumberMap[j] = B
        posCodeCharMap[B] = j
    }
}())


let pro={
    randomGet:function (array) {
        return array[Math.floor(array.length*Math.random())]
    },
    getBodyEnergyNeed:function(body){
        let need=0;
        body.forEach(e=>{if(BODYPART_COST[e])need+=BODYPART_COST[e]});
        return need;
    },
    /*
    let str = Utils.encodePosArray(Memory.rooms.W5N8.structMap.constructedWall.map(e=>{return {x:e[0],y:e[1]}}))
    log(str)
    let arrs = Utils.decodePosArray(str)
    log(arrs)
    log(Memory.rooms.W5N8.structMap.constructedWall.map(e=>{return {x:e[0],y:e[1]}}))
    */
    encodePosArray:function (posArray){
        return posArray.map(e=>posCodeNumberMap[e.x]+posCodeNumberMap[e.y]).reduce((a,b)=>a+b,"")
    },
    decodePosArray:function (string){
        let out = []
        for(let i=0;i<string.length;i+=2){
            out.push({x:posCodeCharMap[string[i]],y:posCodeCharMap[string[i+1]]})
        }
        return out;
    },
    hashCode:function (str){
        let hash = 5381;
        for (let i = 0; i < str.length; i++) {
            let char = str.charCodeAt(i);
            hash = ((hash << 5) + hash) + char; /* hash * 33 + c */
        }
        return hash
    },
    //线性同余随机数
    rnd : function( seed ){
        return ( seed * 9301 + 49297 ) % 233280; //为何使用这三个数?
    },
};


global.Utils=pro;