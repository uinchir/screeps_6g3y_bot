


/**
 * 返回范围内的pos List，不包括自己
 * @param range
 * @returns {[RoomPosition]}
 */
RoomPosition.prototype.nearPos = function (range) {
    range=range||1
    let arr=[];
    for(let i=-range;i<=range;i++){
        for(let j=-range;j<=range;j++){
            if((i||j)&&this.x+i>0&&this.y+j>0&&this.x+i<49&&this.y+j<49)arr.push(new RoomPosition(this.x+i,this.y+j,this.roomName))
        }
    }
    return arr
}
RoomPosition.prototype.isBorder = function () {
    return !(this.x>0&&this.y>0&&this.x<49&&this.y<49)
}
/**
 * 看不见时，只能判定terrain
 * 修墙里的隧道不考虑（不会有人真的修墙里面吧？
 * @param withCreep
 * @returns {boolean}
 */
RoomPosition.prototype.walkable = function (withCreep = false) {
    if(Game.rooms[this.roomName]){
        let structure = this.lookFor(LOOK_STRUCTURES).every(struct => {
            return !(struct.structureType !== STRUCTURE_CONTAINER && struct.structureType !== STRUCTURE_ROAD &&
                (struct.structureType !== STRUCTURE_RAMPART || !struct.my|| struct.isPublic))
        }) && this.lookFor(LOOK_TERRAIN).every(o => o !== 'wall')
        if (withCreep) {
            let creep = (this.lookFor(LOOK_CREEPS).length === 0)
            return structure && creep
        } else {
            return structure
        }
    }else{
        return new Room.Terrain(this.roomName).get(this.x,this.y)!=1 //1 是 wall
    }
}


RoomPosition.prototype.walkableAroundCnt = function (withCreep = false) {
    return this.nearPos(1).reduce((all,pos)=>all+(pos.walkable(withCreep)?1:0),0)
}